const path = require('path');

const PORT = 3000;

const express = require('express');

const bodyParser = require('body-parser');

const app = express();

app.use(express.static('dist', {'index': ['index.html']}));
app.use(express.static('pub'));

app.get('/migrating_to_react16', function (req, res) {
  res.sendFile(path.resolve(__dirname, '../dist/migrating_to_react16.pdf'));
});

app.get('/yo', function (req, res) {
  res.send('yo\n');
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);