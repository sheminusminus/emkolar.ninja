/**
 * @fileOverview Webpack basic configuration file.
 */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const SRC_FOLDER = path.join(__dirname, 'src');
const DIST_FOLDER = path.join(__dirname, 'dist');

module.exports = {
	devtool: '#source-map',
  entry: {
    app: path.join(SRC_FOLDER, 'js/App.js'),
    animation: path.join(SRC_FOLDER, 'static_js/animation.js'),
    exokit: path.resolve(SRC_FOLDER, 'exoapp/index.js'),
  },
  output: {
    publicPath: '/',
    path: DIST_FOLDER,
    filename: 'js/[name].[hash].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: 'babel-loader',
        query: {
          presets: ['es2015', 'react'],
        },
      },
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]_[local]_[hash:base64:5]&sourceMap&-minimize', 'postcss-loader'],
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [
          'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
        ],
      },
    ],
  },
  plugins: [
	  new webpack.NamedModulesPlugin(),
	  new CopyWebpackPlugin([
	    { from: 'src/pages/resume.pdf', to: 'resume.pdf'},
      { from: 'src/pages/migrating_to_react16.pdf', to: 'migrating_to_react16.pdf'},
      { from: 'src/img/favicon.png', to: 'favicon.png' },
      {
          context: 'src/img',
          from: '**/*',
          to: 'img',
      },
	  ]),
	  new HtmlWebpackPlugin({  // also generate an index.html
	    filename: 'index.html',
	    template: 'src/pages/index.html',
	  }),
	],
};

