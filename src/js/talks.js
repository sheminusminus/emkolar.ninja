export const talks = [
  { title: "Migrating to React 16", date: 'November 29, 2017', slides: "/migrating_to_react16.pdf", link: '#' },
];
