export const featured = {title: "Medean", description: "Lead front-end. React, Redux, Node.", url: "https://beta.medean.com", url2: "https://beta.medean.com", image1: "medean.png", image2: "medean-compare.png"};
export const projects = [
	{title: 'Exokit Site', description: 'Web design and development. Vanilla HTML/JS/CSS.', url: 'https://exokitbrowser.com', image: 'exo.png' },
	{title: 'Sweetiebird', description: 'Web design. Vanilla HTML/CSS.', url: 'https://sweetiebird.io', image: 'sweetiebird.png' },
  {title: "MyDigitalGuestbook: Web", description: "Lead developer. Webapp, user-content manager, and online store. React, Node, Express, Firebase.", url: "https://mydigitalguestbook.com", url2: 'https://mydigitalguestbook.com/admin', image: "mdgb-landing.png", mobileImage: "mdgb-m.png", image2: "personalize.png", mobileImage2: "mdgb3-m.png"},
  {title: "MyDigitalGuestbook: iOS", description: "Lead developer. iPad application for creating unique guestbooks. Swift, Firebase.", url: "https://itunes.apple.com/us/app/my-digital-guestbook/id1161749441?mt=8", image: "evt.jpg", mobileImage: "main.jpg"},
	{title: "Greenhouse Studio", description: "Website and client CMS built on Grav. PHP, Twig, JavaScript.", url: "http://dev.greenhouse.leoburnett.com", image: "green.png", mobileImage: "green-m.png"},
	{title: "LBDOD", description: "Website and showcase for the Dept of Design at Leo Burnett. QA and UI work; JavaScript/JQuery, PHP/Twig.", url: "http://lbdod.com", image: "dod.png", mobileImage: "dod-m.png"},
	{title: "Coaster Challenge", description: "Animation-based website and 2D game. JavaScript, SVG, GreenSock, Construct2.", url: "http://coasterchallenge.com", image: "coaster.png", mobileImage: "coaster-m.png"},
	{title: "Self(i.e.) Love Book", description: "Custom wordpress site and intergrations. PHP, JavaScript.", url: "http://selfielovebook.com", image: "selfie.png", mobileImage: "selfie-m.png"},
	{title: "deana.ninja", description: "Professional portfolio site. HTML5, CSS3, JavaScript.", url: "https://deana.ninja", image: "deana.png", mobileImage: "deana-m.png"},
];