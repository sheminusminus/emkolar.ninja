import React from 'react';

class Modal extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			showMe: false
		};
	}

	render() {
		const overlayToggles = this.state.showMe ? 'modal' : 'modal hidden';
		return (
			<div className={overlayToggles}>
				<div className="modal-content">
					I am some content.
				</div>
			</div>
		)
	}
}

export default Modal;