import React from 'react';

class Featured extends React.Component {
	constructor(props) {
		super(props);
		this.openProject = this.openProject.bind(this);
		this.openProjectAlt = this.openProjectAlt.bind(this);
	}	
	
	openProject(evt) {
		evt.preventDefault();
		window.open(this.props.project.url);
	}
	
	openProjectAlt(evt) {
		evt.preventDefault();
		window.open(this.props.project.url2);
	}
	
	render() {
		const { url, title, image1, image2 } = this.props.project;
		const path = '/img/';

		return (
			<div className="featured" id="featured">
				<a className="featuredLink" href={url} target="_blank">{title + ' ' + String.fromCharCode(8690)}</a>
				<div className="featuredImages">
					<div className="mainImage" id="mainImage1" onClick={this.openProject}>
						<img src={path + image1} alt={title} />
					</div>
					<div className="mainImage" id="mainImage2" onClick={this.openProjectAlt}>
						<img src={path + image2} alt={title} />
					</div>
				</div>
			</div>
		)
	}
}

export default Featured;