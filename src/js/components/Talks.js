import React from 'react';

class Talks extends React.Component {
  renderTalks() {
    return this.props.talks.map((talk, idx) => (
      <div key={`talk-${idx}`}>
        <a href={talk.link} target="_blank">{talk.title}</a><br/>
        <span>{talk.date} • </span><a href={talk.slides} target="_blank">Slides</a>
      </div>
    ));
  }

  render() {
    const talks = this.renderTalks();

    return (
      <div className="talks" id="talks">
        <h3>Talks</h3>
        {talks}
      </div>
    )
  }
}

export default Talks;
