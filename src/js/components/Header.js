import React from 'react';
import '../../scss/header.scss';

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: 'Emily Kolar',
			subtitle: 'Web and iOS Developer',
			message: 'View Portfolio'
		};
	}
	
	render() {
		return (
			<div className="header" id="header">
				<h1 className="h1" id="site-title">{this.state.title}</h1>
				<h2 className="h2" id="site-subtitle">{this.state.subtitle}</h2>
				<div className="icons">
					<a href="./resume.pdf" className="iconLink" id="email-link" target="_blank">
						<span className="icon" id="email"></span>
					</a>
					<a href="https://www.linkedin.com/in/emily-kolar-0bb63a54" target="_blank" className="iconLink" id="lin-link">
						<span className="icon" id="lin"></span>
					</a>
					<a href="https://github.com/sheminusminus" target="_blank" className="iconLink" id="hub-link">
						<span className="icon" id="hub"></span>
					</a>
					<a href="https://gitlab.com/sheminusminus" target="_blank" className="iconLink" id="lab-link">
						<span className="icon" id="lab"></span>
					</a>
				</div>				
				<canvas id="header-canvas"></canvas>
				<button className="btn scroll-btn" id="down-arrow" onClick={this.props.scroll}>
					<span className="btn-action">&#x2193;</span>
				</button>
				<div className="opacity-mask"/>
			</div>
		)
	}
}

export default Header;