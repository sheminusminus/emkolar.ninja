import React from 'react';

class Project extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			hovered: false,
		};
		this.openProject = this.openProject.bind(this);
		this.toggleHover = this.toggleHover.bind(this);
	}

	openProject(evt) {
		evt.preventDefault();
		window.open(this.props.url);
	}

	toggleHover() {
		this.setState({
			hovered: !this.state.hovered,
		});
	}
	
	render() {
		const { hovered } = this.state;
		const { classlist, id, url, title, image } = this.props;

		return (
			<div className={classlist} id={id}>
				<a
					onMouseEnter={this.toggleHover}
					onMouseLeave={this.toggleHover}
					className={`projectTitle${hovered ? ' hovered' : ''}`}
					href={url}
					target="_blank">
						{title + ' ' + String.fromCharCode(8690)}
				</a>
				<div className="projectImages" onClick={this.openProject}>
					<div className="mainImage">
						<img src={image} alt={title} />
					</div>
				</div>
			</div>
		)
	}
}

export default Project;