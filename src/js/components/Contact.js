import React from 'react';

class Contact extends React.Component {
	render() {
		return (
			<div className="contact" id="contact">
				<p>Drop me a line?</p><br />
				<p>emkolar@gmail.com</p><br />
				<p>(636) 489 - 9188</p><br />
			</div>
		)
	}
}

export default Contact;