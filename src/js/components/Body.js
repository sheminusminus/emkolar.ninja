import React from 'react';
import Work from './Work';
import Talks from './Talks';

class Body extends React.Component {
	render() {
		const { projects, talks, featuredProject, lightbox } = this.props;

		return (
			<div className="pagebody" id="pagebody">
				<Talks talks={talks} />
				<Work projects={projects} featuredProject={featuredProject} lightbox={lightbox} />
			</div>
		)
	}
}

export default Body;