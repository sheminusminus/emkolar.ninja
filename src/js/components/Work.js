import React from 'react';
import Project from './Project';
import Featured from './Featured';

class Work extends React.Component {
	renderProjects() {
		const projects = [];
		for (var i = 0; i < this.props.projects.length; i++) {
			const imgSrc = '/img/' + this.props.projects[i].image;
			const mobSrc = '/img/' + this.props.projects[i].mobileImage;
			projects.push(<Project key={"k-p" + i} classlist={"project"} id={"project-" + i} title={this.props.projects[i].title} description={this.props.projects[i].description} url={this.props.projects[i].url} image={imgSrc} mobileImage={mobSrc} />);
		}
		return projects;
	}
	
	render() {
		const projects = this.renderProjects();
		return (
			<div className="work" id="work">
				<Featured project={this.props.featuredProject} handleImageClick={this.props.lightbox} />
				{projects}
			</div>
		)
	}
}

export default Work;