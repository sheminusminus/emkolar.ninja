import React from 'react';

class Intro extends React.Component {
	render() {
		return (
			<div style={{ zIndex: '899' }} className="intro" id="intro">
				<p />
				<p>I love to build things.</p>
				<p>For the web, iOS, and beyond.</p>
				<p id="langs">JavaScript + Nodejs + React<br/>
					{"Swift | C# | PHP | SQL | NoSQL"}
				</p>
			</div>
		)
	}
}

export default Intro;