import React from 'react';

class Footer extends React.Component {
	render() {
		const { blurb, copyright } = this.props;

		return (
			<div className="footer" id="footer">
				<p>
					<span className="blurb">{blurb}</span>
					<span className="copyright">{copyright}</span>
				</p>
			</div>
		)
	}
}

export default Footer;