import React from 'react';
import ReactDOM from 'react-dom';
import Intro from './components/Intro';
import Body from './components/Body';
import Header from './components/Header';
import Footer from './components/Footer';

import { featured, projects } from './projects';
import { talks } from './talks';

import '../scss/styles.scss';

class App extends React.Component {
	constructor() {
		super();
		this.state = {
			scrollY: 0,
			copyright: `${String.fromCharCode(169)} Emily Kolar, ${(new Date()).getFullYear()}`,
			blurb: `${String.fromCharCode(10084)} Made in Chicago`
		};
	}
		
	scroll(evt) {
		evt.preventDefault();
		const scrollY = window.innerHeight;
		let yPos = window.scrollY;
		const t = setInterval(function() {
			yPos += 15;
			window.scroll(0, yPos);
			if (yPos + 30 > scrollY) {
				clearInterval(t);
				window.scroll(0, scrollY);
			}
		}, 5);
	}
	
	render() {
		return (
			<div className="topLevel" id="top-level">
				<Header 
					scroll={this.scroll} />
				<Intro />
				<Body 
					projects={projects} 
					featuredProject={featured}
					talks={talks} />
				<Footer 
					copyright={this.state.copyright} 
					blurb={this.state.blurb} />
			</div>
		)
	}
}

if (navigator.userAgent !== 'Exokit') {
  ReactDOM.render(<App />, document.querySelector('#react'));
}
