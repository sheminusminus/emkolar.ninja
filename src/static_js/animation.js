module.exports = (function() {
  if (navigator.userAgent === 'Exokit') return;

	var width, height, header, canvas, ctx, circles, target, animateHeader;

	  // so we don't continue animating if header is out of viewport
	  animateHeader = true;

    initHeader();
    addListeners();

    function initHeader() {
	    // set size constraints
        width = window.innerWidth;
        height = window.innerHeight;
        target = {x: 0, y: height};

        header = document.getElementById('header');
        header.style.height = (height * 1.25) + 'px';

        canvas = document.getElementById('header-canvas');
        canvas.width = width;
        canvas.height = (height * 1.25);
        ctx = canvas.getContext('2d');

		// create circles and draw
        circles = [];
        for(var x = 0; x < width*0.5; x++) {
            var c = new Circle();
            circles.push(c);
        }
        animate();
    }

    // Event handling
    function addListeners() {
        window.addEventListener('scroll', scrollCheck);
        window.addEventListener('resize', resize);
    }

    function scrollCheck() {
        if (document.body.scrollTop > height * 1.25) { animateHeader = false; }
        else { animateHeader = true; }
    }

    function resize() {
        width = window.innerWidth;
        header.style.height = (height * 1.25) + 'px';
        canvas.width = width;
    }
    
    function animate() {
        if(animateHeader) {
            ctx.clearRect(0,0,width,height * 1.25);
            for(var i in circles) {
                circles[i].draw();
            }
        }
        requestAnimationFrame(animate);
    }

function Circle() {
        var _this = this;

        // constructor
        (function() {
            _this.pos = {};
            init();
        })();

        function init() {
            _this.alpha = 0;
            _this.direction = 1;
            _this.pos.x = Math.random()*width;
            _this.pos.y = Math.random() * (height * 1.2) - (Math.random() * (height * 0.3));
            _this.maxAlpha = 0.15+Math.random()*0.25;
            _this.scale = 0.1+Math.random()*0.5;
            _this.velocity = Math.random() + 0.1;
        }

        this.draw = function() {
            _this.pos.y -= _this.velocity;
            _this.alpha += 0.0005 * _this.direction;
            ctx.beginPath();
            ctx.arc(_this.pos.x, _this.pos.y, _this.scale*10, 0, 2 * Math.PI, false);
            ctx.fillStyle = 'rgba(240,240,255,'+ _this.alpha+')';
            ctx.fill();
            if(_this.alpha <= 0) {
              init();
            } else if (_this.alpha >= _this.maxAlpha) {
                _this.direction *= -1;
            }
        };
    }

})();

//             ctx.fillStyle = 'rgba(80,28,100,'+ _this.alpha+')';
